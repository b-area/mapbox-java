package com.mapbox.services.android;

/**
 * Created by antonio on 1/30/16.
 */
public class Constants {

    /*
     * Namespace to avoid constants collision
     */

    private final static String PACKAGE_NAME = "com.mapbox.services.android";

}
