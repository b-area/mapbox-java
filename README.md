[![](https://www.mapbox.com/android-sdk/images/service-splash.png)](https://www.mapbox.com/android-sdk/#mapbox_android_services)

[![Build Status](https://www.bitrise.io/app/a7eea7d04be1e2e5.svg?token=OruuJNhnjyeRnlBv0wXsFQ&branch=master)](https://www.bitrise.io/app/a7eea7d04be1e2e5) [![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.mapbox.mapboxsdk/mapbox-android-services/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.mapbox.mapboxsdk/mapbox-android-services)
<a href="http://www.methodscount.com/?lib=com.mapbox.mapboxsdk%3Amapbox-android-services%3A1.1.0"><img src="https://img.shields.io/badge/Methods count-core: 113 | deps: 30073-e91e63.svg"></img></a>

# Mapbox Android Services (MAS)

An open source toolset for building applications that need directions, geocoding, or static map imagery. Two libraries are at your disposal: a **Java library** compatible with any Java application and an **Android library** intended to be used specifically for Android applications.

### Installation
To install and use MAS in an application, see the [Mapbox Android Services website](https://www.mapbox.com/android-sdk/#mapbox_android_services).

### Sample code

Check the [Test App](https://github.com/mapbox/mapbox-java/tree/master/libandroid/app) for a complete Android demo app using this library. You can also visit the [Mapbox Android SDK examples page](https://www.mapbox.com/android-sdk/examples/) for additional code examples.
