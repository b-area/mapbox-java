package com.mapbox.services.commons;

/**
 * Generic Exception for all things directions
 */
public class ServicesException extends Exception {

    public ServicesException(String message) {
        super(message);
    }

}
