package com.mapbox.services;

public class Constants {

    public final static String BASE_API_URL = "https://api.mapbox.com";

    /*
     * For polyline encoding/decoding
     */

    public final static int GOOGLE_PRECISION = 5;

    public final static int OSRM_PRECISION_V4 = 6;
    public final static int OSRM_PRECISION_V5 = 5;

    /*
     * Mapbox default styles
     * https://www.mapbox.com/developers/api/styles/
     */

    public final static String MAPBOX_USER = "mapbox";

    public final static String MAPBOX_STYLE_STREETS = "streets-v8";
    public final static String MAPBOX_STYLE_LIGHT = "light-v8";
    public final static String MAPBOX_STYLE_DARK = "dark-v8";
    public final static String MAPBOX_STYLE_EMERALD = "emerald-v8";
    public final static String MAPBOX_STYLE_SATELLITE = "satellite-v8";
    public final static String MAPBOX_STYLE_SATELLITE_HYBRID = "satellite-hybrid-v8";

}
