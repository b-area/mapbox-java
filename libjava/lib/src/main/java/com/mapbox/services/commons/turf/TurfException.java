package com.mapbox.services.commons.turf;

/**
 * Created by antonio on 5/12/16.
 */
public class TurfException extends Exception {

    public TurfException(String message) {
        super(message);
    }
}
