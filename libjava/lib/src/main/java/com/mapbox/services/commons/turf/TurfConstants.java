package com.mapbox.services.commons.turf;

/**
 * Created by antonio on 5/12/16.
 */
public class TurfConstants {

    public final static String UNIT_MILES = "miles";
    public final static String UNIT_NAUTICAL_MILES = "nauticalmiles";
    public final static String UNIT_KILOMETERS = "kilometers";
    public final static String UNIT_RADIANS = "radians";
    public final static String UNIT_DEGREES = "degrees";
    public final static String UNIT_INCHES = "inches";
    public final static String UNIT_YARDS = "yards";
    public final static String UNIT_METERS = "meters";

    public final static String UNIT_DEFAULT = UNIT_KILOMETERS;

}
